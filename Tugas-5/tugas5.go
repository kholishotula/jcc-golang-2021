package main

import (
	"fmt"
	"strings"
)

// soal 1

func luasPersegiPanjang(panjang int, lebar int) int {
	return panjang * lebar
}

func kelilingPersegiPanjang(panjang int, lebar int) int {
	return 2 * (panjang + lebar)
}

func volumeBalok(panjang int, lebar int, tinggi int) int {
	return panjang * lebar * tinggi
}

func main() {
	panjang := 12
	lebar := 4
	tinggi := 8

	luas := luasPersegiPanjang(panjang, lebar)
	keliling := kelilingPersegiPanjang(panjang, lebar)
	volume := volumeBalok(panjang, lebar, tinggi)

	fmt.Println(luas)
	fmt.Println(keliling)
	fmt.Println(volume)
}

// -------------------------------------------------------------------------
// soal 2

// func introduce(nama string, jk string, pekerjaan string, usia string) (introduction string) {
// 	jk = strings.ToLower(jk)
// 	if jk == "perempuan" {
// 		introduction = "Bu "
// 	} else if jk == "laki-laki" {
// 		introduction = "Pak "
// 	}
// 	introduction += nama + " adalah seorang " + pekerjaan + " yang berusia " + usia + " tahun"
// 	return
// }

// func main() {
// 	john := introduce("John", "laki-laki", "penulis", "30")
// 	fmt.Println(john) // Menampilkan "Pak John adalah seorang penulis yang berusia 30 tahun"

// 	sarah := introduce("Sarah", "perempuan", "model", "28")
// 	fmt.Println(sarah) // Menampilkan "Bu Sarah adalah seorang model yang berusia 28 tahun"
// }

// -------------------------------------------------------------------------
// soal 3

// func buahFavorit(nama string, namaBuah ...string) string {
// 	kalimat := "halo nama saya " + nama + " dan buah favorit saya adalah "

// 	for _i, buah := range namaBuah {
// 		kalimat += `"` + buah + `"`
// 		if _i != len(namaBuah) {
// 			kalimat += ", "
// 		}
// 	}

// 	return kalimat
// }

// func main() {
// 	var buah = []string{"semangka", "jeruk", "melon", "pepaya"}

// 	var buahFavoritJohn = buahFavorit("John", buah...)

// 	fmt.Println(buahFavoritJohn)
// 	// halo nama saya john dan buah favorit saya adalah "semangka", "jeruk", "melon", "pepaya"
// }

// -------------------------------------------------------------------------
// soal 4

// func main() {
// 	var dataFilm = []map[string]string{}
// 	// buatlah closure function disini
// 	var tambahDataFilm = func(judul string, durasi string, genre string, tahun string) {
// 		dataFilm = append(dataFilm, map[string]string{
// 			"genre": genre,
// 			"jam":   durasi,
// 			"tahun": tahun,
// 			"title": judul,
// 		})
// 	}

// 	tambahDataFilm("LOTR", "2 jam", "action", "1999")
// 	tambahDataFilm("avenger", "2 jam", "action", "2019")
// 	tambahDataFilm("spiderman", "2 jam", "action", "2004")
// 	tambahDataFilm("juon", "2 jam", "horror", "2004")

// 	for _, item := range dataFilm {
// 		fmt.Println(item)
// 	}
// }
