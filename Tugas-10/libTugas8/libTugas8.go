package libtugas8

import "strconv"

type SegitigaSamaSisi struct {
	Alas, Tinggi int
}

type PersegiPanjang struct {
	Panjang, Lebar int
}

type Tabung struct {
	JariJari, Tinggi float64
}

type Balok struct {
	Panjang, Lebar, Tinggi int
}

type Phone struct {
	Name, Brand string
	Year        int
	Colors      []string
}

type HitungBangunDatar interface {
	Luas() int
	Keliling() int
}

type HitungBangunRuang interface {
	Volume() float64
	LuasPermukaan() float64
}

type Description interface {
	ShowDescription() string
}

func (s SegitigaSamaSisi) Luas() int {
	return s.Alas * s.Tinggi / 2
}

func (s SegitigaSamaSisi) Keliling() int {
	return s.Alas * 3
}

func (p PersegiPanjang) Luas() int {
	return p.Panjang * p.Lebar
}

func (p PersegiPanjang) Keliling() int {
	return 2 * (p.Panjang + p.Lebar)
}

func (b Balok) Volume() float64 {
	return float64(b.Panjang * b.Lebar * b.Tinggi)
}

func (b Balok) LuasPermukaan() float64 {
	return float64((2 * (b.Panjang + b.Lebar)) + (2 * (b.Panjang + b.Tinggi)) + (2 * (b.Tinggi + b.Lebar)))
}

func (t Tabung) Volume() float64 {
	if int(t.JariJari)%7 == 0 {
		return float64(22 / 7 * t.JariJari * t.JariJari * t.Tinggi)
	} else {
		return float64(3.14 * t.JariJari * t.JariJari * t.Tinggi)
	}
}

func (t Tabung) LuasPermukaan() float64 {
	if int(t.JariJari)%7 == 0 {
		return float64(2 * 22 / 7 * t.JariJari * (t.JariJari + t.Tinggi))
	} else {
		return float64(2 * 3.14 * t.JariJari * (t.JariJari + t.Tinggi))
	}
}

func (p Phone) ShowDescription() (desc string) {
	var colors string

	for index, item := range p.Colors {
		if index == 0 {
			colors += item
		} else {
			colors += ", " + item
		}
	}

	desc = "name : " + p.Name + "\n" +
		"brand : " + p.Brand + "\n" +
		"year : " + strconv.Itoa(p.Year) + "\n" +
		"color: " + colors
	return
}

func LuasPersegi(sisi uint, tampilkan bool) interface{} {
	switch {
	case sisi == 0 && tampilkan == false:
		return nil
	case sisi == 0 && tampilkan == true:
		return "Maaf anda belum menginput sisi dari persegi"
	case tampilkan == false:
		return sisi * sisi
	default:
		return "luas persegi dengan sisi " + strconv.Itoa(int(sisi)) + "cm adalah " + strconv.Itoa(int(sisi*sisi)) + " cm"
	}
}
