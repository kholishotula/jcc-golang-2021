package main

import (
	"flag"
	"fmt"
	"math"
	"sort"
	"time"
)

// soal 1
func tambahDataPhone(brand string, phones *[]string) {
	*phones = append(*phones, brand)
}

// soal 2
func hitungLuasLingkaran(jari int) (luas float64) {
	luas = math.Round(math.Pi * float64(jari) * float64(jari))
	return
}

func hitungKelilingLingkaran(jari int) (keliling float64) {
	keliling = math.Round(math.Pi * 2 * float64(jari))
	return
}

// soal 3
func hitungLuasPersegiPanjang(panjang int, lebar int) (luas int) {
	luas = panjang * lebar
	return
}

func hitungKelilingPersegiPanjang(panjang int, lebar int) (luas int) {
	luas = 2 * (panjang + lebar)
	return
}

func main() {
	// soal 1
	fmt.Println("Soal 1")
	var phones = []string{}

	tambahDataPhone("Xiaomi", &phones)
	tambahDataPhone("Asus", &phones)
	tambahDataPhone("IPhone", &phones)
	tambahDataPhone("Samsung", &phones)
	tambahDataPhone("Oppo", &phones)
	tambahDataPhone("Realme", &phones)
	tambahDataPhone("Vivo", &phones)

	sort.Strings(phones)
	for i, phone := range phones {
		fmt.Printf("%d. %s\n", i+1, phone)
		time.Sleep(1 * time.Second)
	}

	fmt.Println("============================")
	fmt.Println()

	// soal 2
	fmt.Println("Soal 2")
	fmt.Println("Lingkaran dengan jari-jari 7")
	fmt.Println("Keliling :", hitungKelilingLingkaran(7))
	fmt.Println("Luas :", hitungLuasLingkaran(7))
	fmt.Println()
	fmt.Println("Lingkaran dengan jari-jari 10")
	fmt.Println("Keliling :", hitungKelilingLingkaran(10))
	fmt.Println("Luas :", hitungLuasLingkaran(10))
	fmt.Println()
	fmt.Println("Lingkaran dengan jari-jari 15")
	fmt.Println("Keliling :", hitungKelilingLingkaran(15))
	fmt.Println("Luas :", hitungLuasLingkaran(15))

	fmt.Println("============================")
	fmt.Println()

	// soal 3
	fmt.Println("Soal 3")
	var panjang = flag.Int("panjang", 0, "masukkan panjang")
	var lebar = flag.Int("lebar", 0, "masukkan lebar")

	flag.Parse()
	// go run tugas11.go -panjang=10 -lebar=5
	fmt.Println("Luas persegi panjang :", hitungLuasPersegiPanjang(*panjang, *lebar))
	fmt.Println("Leliling persegi panjang :", hitungKelilingPersegiPanjang(*panjang, *lebar))
}
