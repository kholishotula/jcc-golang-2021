package books

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"log"
	"quiz-3/config"
	"quiz-3/models"
	"strconv"
	"strings"
	"time"
)

const (
	table          = "books"
	layoutDateTime = "2006-01-02 15:04:05"
)

func Get(ctx context.Context, title string, minYear string, maxYear string, minPage string, maxPage string, sort string) ([]models.Book, error) {
	var books []models.Book

	db, err := config.MySQL()

	if err != nil {
		log.Fatal("Cant connect to MySQL", err)
	}

	queryText := fmt.Sprintf("SELECT * FROM %v", table)

	// soal 4
	if title != "" {
		queryText += fmt.Sprintf(" WHERE lower(title) LIKE '%v'", title)
	}
	if minYear != "" {
		// untuk mengecek apakah sebelumnya ada kueri where,
		// kalo ada tinggal tambahi AND
		// kalo gaada buat WHERE baru
		if strings.Contains(queryText, "WHERE") {
			queryText += " AND"
		} else {
			queryText += " WHERE"
		}
		queryText += fmt.Sprintf(" release_year > %v", minYear)
	}
	if maxYear != "" {
		// untuk mengecek apakah sebelumnya ada kueri where,
		// kalo ada tinggal tambahi AND
		// kalo gaada buat WHERE baru
		if strings.Contains(queryText, "WHERE") {
			queryText += " AND"
		} else {
			queryText += " WHERE"
		}
		queryText += fmt.Sprintf(" release_year <= %v", maxYear)
	}
	if minPage != "" {
		// untuk mengecek apakah sebelumnya ada kueri where,
		// kalo ada tinggal tambahi AND
		// kalo gaada buat WHERE baru
		if strings.Contains(queryText, "WHERE") {
			queryText += " AND"
		} else {
			queryText += " WHERE"
		}
		queryText += fmt.Sprintf(" total_page > %v", minPage)
	}
	if maxPage != "" {
		// untuk mengecek apakah sebelumnya ada kueri where,
		// kalo ada tinggal tambahi AND
		// kalo gaada buat WHERE baru
		if strings.Contains(queryText, "WHERE") {
			queryText += " AND"
		} else {
			queryText += " WHERE"
		}
		queryText += fmt.Sprintf(" total_page <= %v", maxPage)
	}
	if sort != "" {
		queryText += fmt.Sprintf(" Order By created_at %v", sort)
	}

	rowQuery, err := db.QueryContext(ctx, queryText)

	if err != nil {
		log.Fatal(err)
	}

	for rowQuery.Next() {
		var book models.Book
		var price, createdAt, updatedAt string
		if err = rowQuery.Scan(&book.ID,
			&book.Title,
			&book.Description,
			&book.ImageURL,
			&book.ReleaseYear,
			&price,
			&book.TotalPage,
			&book.KategoriKetebalan,
			&createdAt,
			&updatedAt); err != nil {
			return nil, err
		}
		book.Price = price

		book.CreatedAt, err = time.Parse(layoutDateTime, createdAt)
		if err != nil {
			log.Fatal(err)
		}

		book.UpdatedAt, err = time.Parse(layoutDateTime, updatedAt)
		if err != nil {
			log.Fatal(err)
		}

		books = append(books, book)
	}
	return books, nil
}

func Insert(ctx context.Context, book models.Book) error {
	db, err := config.MySQL()
	if err != nil {
		log.Fatal("Can't connect to MySQL", err)
	}

	// konversi
	priceInt := strconv.FormatFloat(book.Price.(float64), 'f', -1, 64)
	priceString := "Rp. "
	// konversi input price ke format rupiah
	for i := 0; i < len(priceInt); i++ {
		if (len(priceInt)-i)%3 == 0 && i != 0 {
			priceString += "."
		}
		priceString += string(priceInt[i])
	}
	priceString += ",-"

	queryText := fmt.Sprintf("INSERT INTO %v (title, description, image_url, release_year, price, total_page, kategori_ketebalan, created_at, updated_at) values('%v', '%v', '%v', %v, '%v', '%v', '%v', NOW(), NOW())", table,
		book.Title,
		book.Description,
		book.ImageURL,
		book.ReleaseYear,
		priceString,
		book.TotalPage,
		book.KategoriKetebalan,
	)
	_, err = db.ExecContext(ctx, queryText)

	if err != nil {
		return err
	}
	return nil
}

func Update(ctx context.Context, book models.Book, id string) error {
	db, err := config.MySQL()
	if err != nil {
		log.Fatal("Can't connect to MySQL", err)
	}

	// konversi
	priceInt := strconv.FormatFloat(book.Price.(float64), 'f', -1, 64)
	priceString := "Rp. "
	// konversi input price ke format rupiah
	for i := 0; i < len(priceInt); i++ {
		if (len(priceInt)-i)%3 == 0 && i != 0 {
			priceString += "."
		}
		priceString += string(priceInt[i])
	}
	priceString += ",-"

	queryText := fmt.Sprintf("UPDATE %v set title = '%s', description = '%s', image_url = '%s', release_year = %d, price ='%s', total_page = '%s', kategori_ketebalan = '%s', updated_at = NOW() where id = %s",
		table,
		book.Title,
		book.Description,
		book.ImageURL,
		book.ReleaseYear,
		priceString,
		book.TotalPage,
		book.KategoriKetebalan,
		id,
	)

	_, err = db.ExecContext(ctx, queryText)
	if err != nil {
		return err
	}

	return nil
}

func Delete(ctx context.Context, id string) error {
	db, err := config.MySQL()
	if err != nil {
		log.Fatal("Can't connect to MySQL", err)
	}

	queryText := fmt.Sprintf("DELETE FROM %v where id = %s", table, id)

	s, err := db.ExecContext(ctx, queryText)

	if err != nil && err != sql.ErrNoRows {
		return err
	}

	check, err := s.RowsAffected()
	fmt.Println(check)
	if check == 0 {
		return errors.New("id tidak ada")
	}

	if err != nil {
		fmt.Println(err.Error())
	}

	return nil
}
