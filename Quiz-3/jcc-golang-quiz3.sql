-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 19, 2021 at 10:15 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.3.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `jcc-golang-quiz3`
--

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE `books` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `image_url` varchar(255) NOT NULL,
  `release_year` int(11) NOT NULL,
  `price` varchar(255) NOT NULL,
  `total_page` varchar(255) NOT NULL,
  `kategori_ketebalan` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`id`, `title`, `description`, `image_url`, `release_year`, `price`, `total_page`, `kategori_ketebalan`, `created_at`, `updated_at`) VALUES
(1, 'TLOTR', 'buku the lord of the ring seri awal', 'http://localhost/images/tlotr.png', 1980, 'Rp. 139.000,-', '366', 'tebal', '2021-09-20 00:50:48', '2021-09-20 01:33:57'),
(2, 'TLOTR 2', 'buku the lord of the ring seri 2', 'http://localhost/', 1985, 'Rp. 125.000,-', '365', 'tebal', '2021-09-20 01:08:36', '2021-09-20 01:08:36'),
(3, '10-Day Green Smoothie Cleanse', 'buku 10-Day Green Smoothie Cleanse', 'http://localhost/', 2010, 'Rp. 250.000,-', '120', 'sedang', '2021-09-20 02:35:10', '2021-09-20 02:35:10'),
(4, '11/22/63: A Novel', 'buku tentang 11/22/63: A Novel', 'http://localhost/', 2015, 'Rp. 35.000,-', '74', 'tipis', '2021-09-20 02:35:45', '2021-09-20 02:35:45'),
(5, 'A Dance with Dragons (A Song of Ice and Fire)', 'buku tentang A Dance with Dragons (A Song of Ice and Fire)', 'http://www.google.com/', 1999, 'Rp. 98.000,-', '105', 'sedang', '2021-09-20 02:36:52', '2021-09-20 02:36:52'),
(6, 'A Gentleman in Moscow: A Novel', 'buku tentang A Gentleman in Moscow: A Novel', 'http://www.google.com/', 2017, 'Rp. 105.000,-', '98', 'tipis', '2021-09-20 02:37:28', '2021-09-20 02:37:28'),
(7, 'A Wrinkle in Time (Time Quintet)', 'buku tentang A Wrinkle in Time (Time Quintet)', 'http://www.google.com/', 2018, 'Rp. 285.000,-', '156', 'sedang', '2021-09-20 02:37:53', '2021-09-20 02:37:53');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `books`
--
ALTER TABLE `books`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
