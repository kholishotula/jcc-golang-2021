package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"quiz-3/books"
	"quiz-3/models"
	. "quiz-3/models"
	"quiz-3/utils"
	"strconv"
	"strings"

	"github.com/julienschmidt/httprouter"
)

// soal 2
func hitungSegitiga(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	queryValues := r.URL.Query()
	hitung := queryValues.Get("hitung")

	alas, errAlas := strconv.Atoi(queryValues.Get("alas"))
	if errAlas != nil {
		utils.ResponseJSON(w, "Harap masukkan alas dalam format angka", http.StatusBadRequest)
		return
	}

	tinggi, errTinggi := strconv.Atoi(queryValues.Get("tinggi"))
	if errTinggi != nil {
		utils.ResponseJSON(w, "Harap masukkan tinggi dalam format angka", http.StatusBadRequest)
		return
	}

	chHasilHitung := make(chan int)
	if hitung == "luas" {
		go HitungLuasSegitiga(chHasilHitung, alas, tinggi)
	} else if hitung == "keliling" {
		go HitungKelilingSegitiga(chHasilHitung, alas)
	} else {
		utils.ResponseJSON(w, "Perhitungan "+hitung+" tidak dikenali", http.StatusBadRequest)
		return
	}

	res := map[string]string{
		"status": "Succesfully",
		hitung:   strconv.Itoa(<-chHasilHitung),
	}

	utils.ResponseJSON(w, res, http.StatusOK)
}

func hitungPersegi(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	queryValues := r.URL.Query()
	hitung := queryValues.Get("hitung")

	sisi, errSisi := strconv.Atoi(queryValues.Get("sisi"))
	if errSisi != nil {
		utils.ResponseJSON(w, "Harap masukkan sisi dalam format angka", http.StatusBadRequest)
		return
	}

	chHasilHitung := make(chan int)
	if hitung == "luas" {
		go HitungLuasPersegi(chHasilHitung, sisi)
	} else if hitung == "keliling" {
		go HitungKelilingPersegi(chHasilHitung, sisi)
	} else {
		utils.ResponseJSON(w, "Perhitungan "+hitung+" tidak dikenali", http.StatusBadRequest)
		return
	}

	res := map[string]string{
		"status": "Succesfully",
		hitung:   strconv.Itoa(<-chHasilHitung),
	}

	utils.ResponseJSON(w, res, http.StatusOK)
}

func hitungPersegiPanjang(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	queryValues := r.URL.Query()
	hitung := queryValues.Get("hitung")

	panjang, errPanjang := strconv.Atoi(queryValues.Get("panjang"))
	if errPanjang != nil {
		utils.ResponseJSON(w, "Harap masukkan panjang dalam format angka", http.StatusBadRequest)
		return
	}

	lebar, errLebar := strconv.Atoi(queryValues.Get("lebar"))
	if errLebar != nil {
		utils.ResponseJSON(w, "Harap masukkan lebar dalam format angka", http.StatusBadRequest)
		return
	}

	chHasilHitung := make(chan int)
	if hitung == "luas" {
		go HitungLuasPPanjang(chHasilHitung, panjang, lebar)
	} else if hitung == "keliling" {
		go HitungKelilingPPanjang(chHasilHitung, panjang, lebar)
	} else {
		utils.ResponseJSON(w, "Perhitungan "+hitung+" tidak dikenali", http.StatusBadRequest)
		return
	}

	res := map[string]string{
		"status": "Succesfully",
		hitung:   strconv.Itoa(<-chHasilHitung),
	}

	utils.ResponseJSON(w, res, http.StatusOK)
}

func hitungLingkaran(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	queryValues := r.URL.Query()
	hitung := queryValues.Get("hitung")

	jariJari, errJari := strconv.Atoi(queryValues.Get("jariJari"))
	if errJari != nil {
		utils.ResponseJSON(w, "Harap masukkan jari-jari dalam format angka", http.StatusBadRequest)
		return
	}

	chHasilHitung := make(chan float64)
	if hitung == "luas" {
		go HitungLuasLingkaran(chHasilHitung, jariJari)
	} else if hitung == "keliling" {
		go HitungKelilingLingkaran(chHasilHitung, jariJari)
	} else {
		utils.ResponseJSON(w, "Perhitungan "+hitung+" tidak dikenali", http.StatusBadRequest)
		return
	}

	res := map[string]string{
		"status": "Succesfully",
		hitung:   strconv.FormatFloat(<-chHasilHitung, 'f', 4, 64),
	}

	utils.ResponseJSON(w, res, http.StatusOK)
}

func hitungJajarGenjang(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	queryValues := r.URL.Query()
	hitung := queryValues.Get("hitung")

	alas, errAlas := strconv.Atoi(queryValues.Get("alas"))
	if errAlas != nil {
		utils.ResponseJSON(w, "Harap masukkan alas dalam format angka", http.StatusBadRequest)
		return
	}

	tinggi, errTinggi := strconv.Atoi(queryValues.Get("tinggi"))
	if errTinggi != nil {
		utils.ResponseJSON(w, "Harap masukkan tinggi dalam format angka", http.StatusBadRequest)
		return
	}

	sisi, errSisi := strconv.Atoi(queryValues.Get("sisi"))
	if errSisi != nil {
		utils.ResponseJSON(w, "Harap masukkan sisi dalam format angka", http.StatusBadRequest)
		return
	}

	chHasilHitung := make(chan int)
	if hitung == "luas" {
		go HitungLuasJajar(chHasilHitung, alas, tinggi)
	} else if hitung == "keliling" {
		go HitungKelilingJajar(chHasilHitung, alas, sisi)
	} else {
		utils.ResponseJSON(w, "Perhitungan "+hitung+" tidak dikenali", http.StatusBadRequest)
		return
	}

	res := map[string]string{
		"status": "Succesfully",
		hitung:   strconv.Itoa(<-chHasilHitung),
	}

	utils.ResponseJSON(w, res, http.StatusOK)
}

// soal 3
func GetBook(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	var bukus []Book
	var err error

	queryValues := r.URL.Query()

	title := strings.ToLower(queryValues.Get("title"))
	minYear := queryValues.Get("minYear")
	maxYear := queryValues.Get("maxYear")
	minPage := queryValues.Get("minPage")
	maxPage := queryValues.Get("maxPage")
	sort := queryValues.Get("sort")

	bukus, err = books.Get(ctx, title, minYear, maxYear, minPage, maxPage, sort)
	if err != nil {
		utils.ResponseJSON(w, err, http.StatusInternalServerError)
		return
	}

	utils.ResponseJSON(w, bukus, http.StatusOK)
}

func PostBook(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	if r.Header.Get("Content-Type") != "application/json" {
		http.Error(w, "Gunakan content type application / json", http.StatusBadRequest)
		return
	}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	var book models.Book
	if err := json.NewDecoder(r.Body).Decode(&book); err != nil {
		utils.ResponseJSON(w, err, http.StatusBadRequest)
		return
	}

	// validasi
	var errorValid []string
	// image_url harus merupakan url
	_, errUrl := url.ParseRequestURI(book.ImageURL)
	if errUrl != nil {
		errorValid = append(errorValid, errUrl.Error())
	}
	// release_year di antara 1980 - 2021
	if book.ReleaseYear < 1980 || book.ReleaseYear > 2021 {
		errorValid = append(errorValid, "Buku yang dimasukkan harus berada dalam rentang tahun rilis 1980 - 2021")
	}
	// image_url bukan url dan release year tidak dalam rentang
	if len(errorValid) > 0 {
		utils.ResponseJSON(w, errorValid, http.StatusBadRequest)
		return
	}

	// kategori ketebalan
	totalPage, _ := strconv.Atoi(book.TotalPage)
	if totalPage <= 100 {
		book.KategoriKetebalan = "tipis"
	} else if totalPage <= 200 {
		book.KategoriKetebalan = "sedang"
	} else {
		book.KategoriKetebalan = "tebal"
	}

	if err := books.Insert(ctx, book); err != nil {
		utils.ResponseJSON(w, err, http.StatusInternalServerError)
		return
	}

	res := map[string]string{
		"status": "Succesfully",
	}

	utils.ResponseJSON(w, res, http.StatusCreated)
}

func UpdateBook(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	if r.Header.Get("Content-Type") != "application/json" {
		http.Error(w, "Gunakan content type application / json", http.StatusBadRequest)
		return
	}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	var book models.Book

	if err := json.NewDecoder(r.Body).Decode(&book); err != nil {
		utils.ResponseJSON(w, err, http.StatusBadRequest)
		return
	}

	// validasi
	var errorValid []string
	// image_url harus merupakan url
	_, errUrl := url.ParseRequestURI(book.ImageURL)
	if errUrl != nil {
		errorValid = append(errorValid, errUrl.Error())
	}
	// release_year di antara 1980 - 2021
	if book.ReleaseYear < 1980 || book.ReleaseYear > 2021 {
		errorValid = append(errorValid, "Buku yang dimasukkan harus berada dalam rentang tahun rilis 1980 - 2021")
	}
	// image_url bukan url dan release year tidak dalam rentang
	if len(errorValid) > 0 {
		utils.ResponseJSON(w, errorValid, http.StatusBadRequest)
		return
	}

	// kategori ketebalan
	totalPage, _ := strconv.Atoi(book.TotalPage)
	if totalPage <= 100 {
		book.KategoriKetebalan = "tipis"
	} else if totalPage <= 200 {
		book.KategoriKetebalan = "sedang"
	} else {
		book.KategoriKetebalan = "tebal"
	}

	var idBook = ps.ByName("id")

	if err := books.Update(ctx, book, idBook); err != nil {
		utils.ResponseJSON(w, err, http.StatusInternalServerError)
		return
	}

	res := map[string]string{
		"status": "Succesfully",
	}

	utils.ResponseJSON(w, res, http.StatusCreated)
}

func DeleteBook(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	var idBook = ps.ByName("id")
	if err := books.Delete(ctx, idBook); err != nil {
		errors := map[string]string{
			"error": fmt.Sprintf("%v", err),
		}
		utils.ResponseJSON(w, errors, http.StatusInternalServerError)
		return
	}
	res := map[string]string{
		"status": "Succesfully",
	}
	utils.ResponseJSON(w, res, http.StatusOK)
}

// soal 5
func BasicAuth(h httprouter.Handle, auth map[string]string) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		// Get the Basic Authentication credentials
		user, password, hasAuth := r.BasicAuth()

		var passwordAuth, isExist = auth[user]

		if hasAuth && isExist && password == passwordAuth {
			// Delegate request to the given handle
			h(w, r, ps)
		} else {
			// Request Basic Authentication otherwise
			w.Header().Set("WWW-Authenticate", "Basic realm=Restricted")
			http.Error(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
		}
	}
}

func main() {
	var auth = map[string]string{
		"admin":   "password",
		"editor":  "secret",
		"trainer": "rahasia",
	}

	router := httprouter.New()

	// soal 2
	router.GET("/bangun-datar/segitiga-sama-sisi", hitungSegitiga)
	router.GET("/bangun-datar/persegi", hitungPersegi)
	router.GET("/bangun-datar/persegi-panjang", hitungPersegiPanjang)
	router.GET("/bangun-datar/lingkaran", hitungLingkaran)
	router.GET("/bangun-datar/jajar-genjang", hitungJajarGenjang)

	// soal 3
	router.GET("/books", BasicAuth(GetBook, auth))
	router.POST("/books/create", PostBook)
	router.PUT("/books/:id/update", BasicAuth(UpdateBook, auth))
	router.DELETE("/books/:id/delete", BasicAuth(DeleteBook, auth))

	fmt.Println("Server Running at Port 8080")
	log.Fatal(http.ListenAndServe(":8080", router))
}
