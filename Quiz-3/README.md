## Quiz-3

#### Setup package

Terdapat pada file go.mod line 6 dan 7

#### Membuat API endpoint bangun-datar

- route

Terdapat pada file main.go line 367-371

- handler function

Terdapat pada file main.go line 21-183

- fungsi perhitungan luas dan keliling

Terdapat pada file models/bangun-datar.go

#### CRUD Books

- route

Terdapat pada file main.go line 374-377

- handler function

Terdapat pada file main.go line 186-336

- model

Terdapat pada file models/book.go

- query mysql

Terdapat pada file books/repository_mysql.go

- validasi image_url

Terdapat pada file main.go line 229-232 (insert data) dan 283-286 (update data)

- validasi release_year

Terdapat pada file main.go line 234-236 (insert data) dan 288-290 (update data)

- validasi image_url dan release_year dimunculkan keduanya, tidak salah satu

Error validasi dimasukkan kedalam array, lalu array tersebut ditampilkan jika ada error. Selengkapnya pada file main.go line 238-241 (insert data) dan 292-295 (update data)

- konversi price ke format rupiah

Terdapat pada file books/repository_mysql.go line 129-138 (insert data) dan 164-173 (update data)

- inisialisasi kategori_ketebalan berdasarkan total_page

Terdapat pada file main.go line 244-251 (insert data) dan 298-305 (update data)

#### Filter Books

- mendapatkan query params

Terdapat pada file main.go line 193-200

- menggunakan query params untuk filter data

Terdapat pada file books/repository_mysql.go line 33-82

#### Middleware Basic Auth

- inisialisasi data user (username dan password)

Terdapat pada file main.go line 357-362

- auth routes untuk read, update, dan delete

Terdapat pada file main.go line 374, 376, 377

- fungsi autentikasi

Terdapat pada file main.go line 339-355

#### Export file SQL

Terdapat pada file jcc-golang-quiz3.sql
