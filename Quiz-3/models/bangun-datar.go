package models

import "math"

func HitungLuasSegitiga(chHasilHitung chan int, alas int, tinggi int) {
	luas := alas * tinggi / 2
	chHasilHitung <- luas
}

func HitungKelilingSegitiga(chHasilHitung chan int, alas int) {
	keliling := 3 * alas
	chHasilHitung <- keliling
}

func HitungLuasPersegi(chHasilHitung chan int, sisi int) {
	luas := sisi * sisi
	chHasilHitung <- luas
}

func HitungKelilingPersegi(chHasilHitung chan int, sisi int) {
	keliling := 4 * sisi
	chHasilHitung <- keliling
}

func HitungLuasPPanjang(chHasilHitung chan int, panjang int, lebar int) {
	luas := panjang * lebar
	chHasilHitung <- luas
}

func HitungKelilingPPanjang(chHasilHitung chan int, panjang int, lebar int) {
	keliling := 2 * (panjang + lebar)
	chHasilHitung <- keliling
}

func HitungLuasLingkaran(chHasilHitung chan float64, jari int) {
	luas := math.Pi * float64(jari) * float64(jari)
	chHasilHitung <- luas
}

func HitungKelilingLingkaran(chHasilHitung chan float64, jari int) {
	keliling := math.Pi * 2 * float64(jari)
	chHasilHitung <- keliling
}

func HitungLuasJajar(chHasilHitung chan int, alas int, tinggi int) {
	luas := alas * tinggi
	chHasilHitung <- luas
}

func HitungKelilingJajar(chHasilHitung chan int, alas int, sisi int) {
	keliling := 2 * (alas + sisi)
	chHasilHitung <- keliling
}
