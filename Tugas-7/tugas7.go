package main

import "fmt"

// soal 1
type buah struct {
	nama       string
	warna      string
	adaBijinya bool
	harga      int
}

func (b buah) printBuah() {
	fmt.Println("Nama :", b.nama)
	fmt.Println("Warna :", b.warna)
	if b.adaBijinya == true {
		fmt.Println("Ada Bijinya : Ada")
	} else {
		fmt.Println("Ada Bijinya : Tidak")
	}
	fmt.Println("Harga :", b.harga)
	fmt.Println()
}

// soal 2
type segitiga struct {
	alas, tinggi int
}

type persegi struct {
	sisi int
}

type persegiPanjang struct {
	panjang, lebar int
}

func (s segitiga) luasSegitiga() int {
	return s.alas * s.tinggi / 2
}

func (p persegi) luasPersegi() int {
	return p.sisi * p.sisi
}

func (pp persegiPanjang) luasPersegiPanjang() int {
	return pp.panjang * pp.lebar
}

// soal 3
type phone struct {
	name, brand string
	year        int
	colors      []string
}

func (p *phone) tambahWarna(warna string) {
	*&p.colors = append(*&p.colors, warna)
}

// soal 4
type movie struct {
	title    string
	genre    string
	duration int
	year     int
}

func tambahDataFilm(title string, duration int, genre string, year int, dataFilm *[]movie) {
	var film = movie{title, genre, duration, year}
	*dataFilm = append(*dataFilm, film)
}

func main() {
	// soal 1
	fmt.Println("Soal 1")
	var buahNanas = buah{"Nanas", "Kuning", false, 9000}
	var buahJeruk = buah{"Jeruk", "Oranye", true, 8000}
	var buahSemangka = buah{"Semangka", "Hijau & Merah", true, 10000}
	var buahPisang = buah{"Pisang", "Kuning", false, 5000}

	buahNanas.printBuah()
	buahJeruk.printBuah()
	buahSemangka.printBuah()
	buahPisang.printBuah()

	// soal 2
	fmt.Println("Soal 2")
	var s = segitiga{6, 5}
	var p = persegi{6}
	var pp = persegiPanjang{10, 4}

	fmt.Println("Luas Segitiga :", s.luasSegitiga())
	fmt.Println("Luas Persegi :", p.luasPersegi())
	fmt.Println("Luas Persgei Panjang :", pp.luasPersegiPanjang())
	fmt.Println()

	// soal 3
	fmt.Println("Soal 3")
	var cellphone phone
	cellphone.name = "Samsung M21"
	cellphone.brand = "Samsung"
	cellphone.year = 2020
	cellphone.tambahWarna("blue")
	cellphone.tambahWarna("black")
	cellphone.tambahWarna("silver")

	fmt.Println("Nama HP :", cellphone.name)
	fmt.Println("Merk HP :", cellphone.brand)
	fmt.Println("Tahun rilis HP :", cellphone.year)
	fmt.Printf("Warna Hp : ")
	for _, color := range cellphone.colors {
		fmt.Printf("%s ", color)
	}
	fmt.Println()
	fmt.Println()

	// soal 4
	fmt.Println("Soal 4")
	var dataFilm = []movie{}

	tambahDataFilm("LOTR", 120, "action", 1999, &dataFilm)
	tambahDataFilm("avenger", 120, "action", 2019, &dataFilm)
	tambahDataFilm("spiderman", 120, "action", 2004, &dataFilm)
	tambahDataFilm("juon", 120, "horror", 2004, &dataFilm)

	for _i := range dataFilm {
		fmt.Printf("%d. ", _i+1)
		fmt.Printf("title : %s\n", dataFilm[_i].title)
		fmt.Printf("   duration : %d jam\n", dataFilm[_i].duration/60)
		fmt.Printf("   genre : %s\n", dataFilm[_i].genre)
		fmt.Printf("   year : %d\n\n", dataFilm[_i].year)
	}
}
