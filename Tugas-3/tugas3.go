package main

import (
	"fmt"
	"strconv"
	"time"
)

func main() {
	// soal 1
	fmt.Println("Soal 1")

	var panjangPersegiPanjang string = "8"
	var lebarPersegiPanjang string = "5"

	var alasSegitiga string = "6"
	var tinggiSegitiga string = "7"

	var panjangPP, errPanjangPP = strconv.Atoi(panjangPersegiPanjang)
	var lebarPP, errLebarPP = strconv.Atoi(lebarPersegiPanjang)
	if errPanjangPP == nil && errLebarPP == nil {
		var kelilingPersegiPanjang int = 2*panjangPP + 2*lebarPP
		fmt.Println("Keliling Persegi Panjang dengan panjang =", panjangPP, "dan lebar =", lebarPP, "adalah", kelilingPersegiPanjang)
	}

	var alasS3, errAlasS3 = strconv.Atoi(alasSegitiga)
	var tinggiS3, errTinggiS3 = strconv.Atoi(tinggiSegitiga)
	if errAlasS3 == nil && errTinggiS3 == nil {
		var luasSegitiga int = alasS3 * tinggiS3 / 2
		fmt.Println("Luas Segitiga dengan alas =", alasS3, "dan tinggi =", tinggiS3, "adalah", luasSegitiga)
	}
	fmt.Println("================================")

	// soal 2
	fmt.Println("Soal 2")

	var nilaiJohn = 80
	var nilaiDoe = 50

	if nilaiJohn >= 80 {
		fmt.Println("John indeksnya A")
	} else if nilaiJohn >= 70 {
		fmt.Println("John indeksnya B")
	} else if nilaiJohn >= 60 {
		fmt.Println("John indeksnya C")
	} else if nilaiJohn >= 50 {
		fmt.Println("John indeksnya D")
	} else {
		fmt.Println("John indeksnya E")
	}

	if nilaiDoe >= 80 {
		fmt.Println("Doe indeksnya A")
	} else if nilaiDoe >= 70 {
		fmt.Println("Doe indeksnya B")
	} else if nilaiDoe >= 60 {
		fmt.Println("Doe indeksnya C")
	} else if nilaiDoe >= 50 {
		fmt.Println("Doe indeksnya D")
	} else {
		fmt.Println("Doe indeksnya E")
	}
	fmt.Println("================================")

	// soal 3
	fmt.Println("Soal 3")

	var tanggal = 26
	var bulan = 3
	var tahun = 1999

	switch bulan {
	case 1:
		fmt.Println(tanggal, "Januari", tahun)
	case 2:
		fmt.Println(tanggal, "Februari", tahun)
	case 3:
		fmt.Println(tanggal, "Maret", tahun)
	case 4:
		fmt.Println(tanggal, "April", tahun)
	case 5:
		fmt.Println(tanggal, "Mei", tahun)
	case 6:
		fmt.Println(tanggal, "Juni", tahun)
	case 7:
		fmt.Println(tanggal, "Juli", tahun)
	case 8:
		fmt.Println(tanggal, "Agustus", tahun)
	case 9:
		fmt.Println(tanggal, "September", tahun)
	case 10:
		fmt.Println(tanggal, "Oktober", tahun)
	case 11:
		fmt.Println(tanggal, "November", tahun)
	case 12:
		fmt.Println(tanggal, "Desember", tahun)
	}
	fmt.Println("================================")

	// soal 4
	fmt.Println("Soal 4")

	if tahun >= 1995 && tahun <= 2015 {
		fmt.Println("Anda Generasi Z, karena kelahiran 1995 s.d. 2015")
	} else if tahun >= 1980 {
		fmt.Println("Anda Generasi Y (Millenials), karena kelahiran 1980 s.d. 1994")
	} else if tahun >= 1965 {
		fmt.Println("Anda Generasi X, karena kelahiran 1965 s.d. 1979")
	} else if tahun >= 1944 {
		fmt.Println("Anda BabyBoomer, karena kelahiran 1944 s.d. 1964")
	}

	time.Sleep(5 * time.Second)
}
