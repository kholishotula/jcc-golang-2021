package models

import "time"

type (
	NilaiMahasiswa struct {
		ID          int       `json:"id"`
		Nama        string    `json:"nama"`
		MataKuliah  string    `json:"matkul"`
		Nilai       int       `json:"nilai"`
		IndeksNilai string    `json:"indeks"`
		CreatedAt   time.Time `json:"created_at"`
		UpdatedAt   time.Time `json:"updated_at"`
	}
)
