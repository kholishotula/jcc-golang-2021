package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"tugas-17/models"
	nilaimhs "tugas-17/nilai_mhs"
	"tugas-17/utils"

	"github.com/julienschmidt/httprouter"
)

func GetNilai(w http.ResponseWriter, _ *http.Request, _ httprouter.Params) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	nilais, err := nilaimhs.GetAll(ctx)

	if err != nil {
		fmt.Println(err)
	}

	utils.ResponseJSON(w, nilais, http.StatusOK)
}

func CreateNilai(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	if r.Header.Get("Content-Type") != "application/json" {
		http.Error(w, "Gunakan content type application / json", http.StatusBadRequest)
		return
	}
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	var nilai models.NilaiMahasiswa
	if err := json.NewDecoder(r.Body).Decode(&nilai); err != nil {
		utils.ResponseJSON(w, err, http.StatusBadRequest)
		return
	}

	var indeks string
	if nilai.Nilai > 100 {
		w.Write([]byte("Nilai yang diisi tidak boleh melebihi 100"))
		return
	} else if nilai.Nilai >= 80 {
		indeks = "A"
	} else if nilai.Nilai >= 70 {
		indeks = "B"
	} else if nilai.Nilai >= 60 {
		indeks = "C"
	} else if nilai.Nilai >= 50 {
		indeks = "D"
	} else {
		indeks = "E"
	}
	nilai.IndeksNilai = indeks

	if err := nilaimhs.Insert(ctx, nilai); err != nil {
		utils.ResponseJSON(w, err, http.StatusInternalServerError)
		return
	}
	res := map[string]string{
		"status": "Succesfully",
	}
	utils.ResponseJSON(w, res, http.StatusCreated)
}

func UpdateNilai(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	if r.Header.Get("Content-Type") != "application/json" {
		http.Error(w, "Gunakan content type application / json", http.StatusBadRequest)
		return
	}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	var nilai models.NilaiMahasiswa

	if err := json.NewDecoder(r.Body).Decode(&nilai); err != nil {
		utils.ResponseJSON(w, err, http.StatusBadRequest)
		return
	}

	var indeks string
	if nilai.Nilai > 100 {
		w.Write([]byte("Nilai yang diisi tidak boleh melebihi 100"))
		return
	} else if nilai.Nilai >= 80 {
		indeks = "A"
	} else if nilai.Nilai >= 70 {
		indeks = "B"
	} else if nilai.Nilai >= 60 {
		indeks = "C"
	} else if nilai.Nilai >= 50 {
		indeks = "D"
	} else {
		indeks = "E"
	}
	nilai.IndeksNilai = indeks

	var idNilai = ps.ByName("id")

	if err := nilaimhs.Update(ctx, nilai, idNilai); err != nil {
		utils.ResponseJSON(w, err, http.StatusInternalServerError)
		return
	}

	res := map[string]string{
		"status": "Succesfully",
	}

	utils.ResponseJSON(w, res, http.StatusCreated)
}

func DestroyNilai(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	var idNilai = ps.ByName("id")
	if err := nilaimhs.Delete(ctx, idNilai); err != nil {
		errors := map[string]string{
			"error": fmt.Sprintf("%v", err),
		}
		utils.ResponseJSON(w, errors, http.StatusInternalServerError)
		return
	}
	res := map[string]string{
		"status": "Succesfully",
	}
	utils.ResponseJSON(w, res, http.StatusOK)
}

func main() {
	router := httprouter.New()
	router.GET("/nilai", GetNilai)
	router.POST("/nilai/post", CreateNilai)
	router.PUT("/nilai/:id/update", UpdateNilai)
	router.DELETE("/nilai/:id/delete", DestroyNilai)

	fmt.Println("Server Running at Port 8080")
	log.Fatal(http.ListenAndServe(":8080", router))
}
