package main

import (
	"fmt"
)

// soal 1
func main() {
	fmt.Println("Soal 1")

	var luasLigkaran float64
	var kelilingLingkaran float64

	updateLuas(&luasLigkaran, 10)
	updateKeliling(&kelilingLingkaran, 10)

	fmt.Println("Luas lingkaran", luasLigkaran)
	fmt.Println("Keliling lingkaran", kelilingLingkaran)
}

func updateLuas(luasLigkaran *float64, jari float64) {
	*luasLigkaran = 3.14 * jari * jari
}

func updateKeliling(kelilingLingkaran *float64, jari float64) {
	*kelilingLingkaran = 2 * 3.14 * jari
}

// soal 2
// func main() {
// 	fmt.Println("Soal 2")
// 	var sentence string
// 	introduce(&sentence, "John", "laki-laki", "penulis", "30")

// 	fmt.Println(sentence) // Menampilkan "Pak John adalah seorang penulis yang berusia 30 tahun"
// 	introduce(&sentence, "Sarah", "perempuan", "model", "28")

// 	fmt.Println(sentence) // Menampilkan "Bu Sarah adalah seorang model yang berusia 28 tahun"
// }

// func introduce(sentence *string, nama, jk, pekerjaan, usia string) {
// 	jk = strings.ToLower(jk)
// 	if jk == "perempuan" {
// 		*sentence = "Bu "
// 	} else if jk == "laki-laki" {
// 		*sentence = "Pak "
// 	}
// 	*sentence += nama + " adalah seorang " + pekerjaan + " yang berusia " + usia + " tahun"
// }

// soal 3
// func main() {
// 	fmt.Println("Soal 3")
// 	var buah = []string{}

// 	addBuah(&buah, "Jeruk")
// 	addBuah(&buah, "Semangka")
// 	addBuah(&buah, "Mangga")
// 	addBuah(&buah, "Strawberry")
// 	addBuah(&buah, "Durian")
// 	addBuah(&buah, "Manggis")
// 	addBuah(&buah, "Alpukat")

// 	for i := range buah {
// 		fmt.Printf("%d. %s\n", i+1, buah[i])
// 	}
// }

// func addBuah(buah *[]string, namaBuah string) {
// 	*buah = append(*buah, namaBuah)
// }

// soal 4
// func main() {
// 	fmt.Println("Soal 4")
// 	var dataFilm = []map[string]string{}

// 	tambahDataFilm("LOTR", "2 jam", "action", "1999", &dataFilm)
// 	tambahDataFilm("avenger", "2 jam", "action", "2019", &dataFilm)
// 	tambahDataFilm("spiderman", "2 jam", "action", "2004", &dataFilm)
// 	tambahDataFilm("juon", "2 jam", "horror", "2004", &dataFilm)

// 	for _i := range dataFilm {
// 		fmt.Printf("%d. ", _i+1)
// 		fmt.Printf("title : %s\n", dataFilm[_i]["title"])
// 		fmt.Printf("   duration : %s\n", dataFilm[_i]["duration"])
// 		fmt.Printf("   genre : %s\n", dataFilm[_i]["genre"])
// 		fmt.Printf("   year : %s\n\n", dataFilm[_i]["year"])
// 	}
// }

// func tambahDataFilm(judul string, durasi string, genre string, tahun string, dataFilm *[]map[string]string) {
// 	*dataFilm = append(*dataFilm, map[string]string{
// 		"title":    judul,
// 		"duration": durasi,
// 		"genre":    genre,
// 		"year":     tahun,
// 	})
// }
