package main

import "fmt"

func main() {
	// soal 1
	fmt.Println("Soal 1")
	for i := 1; i <= 20; i++ {
		if i%3 == 0 && i%2 == 1 {
			fmt.Println(i, "- I Love Coding")
		} else if i%2 == 1 {
			fmt.Println(i, "- JCC")
		} else {
			fmt.Println(i, "- Candradimuka")
		}
	}
	fmt.Println("============================")

	// soal 2
	fmt.Println("Soal 2")
	for i := 0; i < 7; i++ {
		for j := 0; j <= i; j++ {
			fmt.Printf("#")
		}
		fmt.Printf("\n")
	}
	fmt.Println("============================")

	// soal 3
	fmt.Println("Soal 3")
	var kalimat = [...]string{"aku", "dan", "saya", "sangat", "senang", "belajar", "golang"}

	var kalimatBaru = kalimat[2:]
	fmt.Println(kalimatBaru)
	fmt.Println("============================")

	// soal 4
	fmt.Println("Soal 4")
	var sayuran = []string{}

	sayuran = append(sayuran, "Bayam")
	sayuran = append(sayuran, "Buncis")
	sayuran = append(sayuran, "Kangkung")
	sayuran = append(sayuran, "Kubis")
	sayuran = append(sayuran, "Seledri")
	sayuran = append(sayuran, "Tauge")
	sayuran = append(sayuran, "Timun")

	for i := range sayuran {
		fmt.Printf("%d. %s\n", i+1, sayuran[i])
	}
	fmt.Println("============================")

	// soal 5
	fmt.Println("Soal 5")
	var satuan = map[string]int{
		"panjang": 7,
		"lebar":   4,
		"tinggi":  6,
	}
	var volumeBalok = 1

	for key, val := range satuan {
		fmt.Println(key, "=", val)
		volumeBalok *= val
	}
	fmt.Println("Volume Balok =", volumeBalok)

}
