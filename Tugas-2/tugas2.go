package main

import (
	"fmt"
	"strconv"
	"strings"
)

func main() {
	// saol 1
	var kata1 string = "Jabar"
	var kata2 string = "Coding"
	var kata3 string = "Camp"
	var kata4 string = "Golang"
	var kata5 string = "2021"
	fmt.Printf("%s %s %s %s %s\n", kata1, kata2, kata3, kata4, kata5)

	// soal 2
	halo := "Halo Dunia"
	halo = strings.ReplaceAll(halo, "Dunia", "Golang")
	fmt.Println(halo)

	// soal 3
	var kataPertama = "saya"
	var kataKedua = "senang"
	var kataKetiga = "belajar"
	var kataKeempat = "golang"

	kataKedua = strings.Title(kataKedua)
	kataKetiga = strings.ReplaceAll(kataKetiga, "r", "R")
	kataKeempat = strings.ToUpper(kataKeempat)

	fmt.Printf("%s %s %s %s\n", kataPertama, kataKedua, kataKetiga, kataKeempat)

	// soal 4
	var angkaPertama = "8"
	var angkaKedua = "5"
	var angkaKetiga = "6"
	var angkaKeempat = "7"
	var sum = 0

	var num1, err1 = strconv.Atoi(angkaPertama)
	if err1 == nil {
		sum += num1
	}
	var num2, err2 = strconv.Atoi(angkaKedua)
	if err2 == nil {
		sum += num2
	}
	var num3, err3 = strconv.Atoi(angkaKetiga)
	if err3 == nil {
		sum += num3
	}
	var num4, err4 = strconv.Atoi(angkaKeempat)
	if err4 == nil {
		sum += num4
	}

	fmt.Println(sum)

	// soal 5
	kalimat := "halo halo bandung"
	angka := 2021

	kalimat = strings.ReplaceAll(kalimat, "halo", "Hi")

	fmt.Printf(`"%s" - %d`, kalimat, angka)
}
