package main

import (
	"fmt"
	"math"
	"sort"
	"sync"
	"time"
)

// soal 1
func printPhone(i int, phone string, wg *sync.WaitGroup) {
	fmt.Printf("%d. %s\n", i, phone)
	time.Sleep(1 * time.Second)
	wg.Done()
}

// soal 2
func getMovies(moviesChannel chan string, movies ...string) {
	for _, movie := range movies {
		moviesChannel <- movie
	}
	close(moviesChannel)
}

// soal 3
func hitungLuasLingkaran(chLuasLingk chan float64, jari int) {
	luas := math.Round(math.Pi * float64(jari) * float64(jari))
	chLuasLingk <- luas
}

func hitungKelilingLingkaran(chKelilingLingk chan float64, jari int) {
	keliling := math.Round(math.Pi * 2 * float64(jari))
	chKelilingLingk <- keliling
}

func hitungVolumeTabung(chVolumeTab chan float64, jari int, tinggi int) {
	volume := math.Round(math.Pi * float64(jari) * float64(jari) * float64(tinggi))
	chVolumeTab <- volume
}

// soal 4
func hitungLuasPersegiPanjang(chLuasPP chan int, panjang int, lebar int) {
	luas := panjang * lebar
	chLuasPP <- luas
}

func hitungKelilingPersegiPanjang(chKelilingPP chan int, panjang int, lebar int) {
	keliling := 2 * (panjang + lebar)
	chKelilingPP <- keliling
}

func hitungVolumeBalok(chVolumeBal chan int, panjang int, lebar int, tinggi int) {
	volume := panjang * lebar * tinggi
	chVolumeBal <- volume
}

func main() {
	// soal 1
	fmt.Println("Soal 1")
	var phones = []string{"Xiaomi", "Asus", "Iphone", "Samsung", "Oppo", "Realme", "Vivo"}

	sort.Strings(phones)

	var wg sync.WaitGroup
	for i, phone := range phones {
		wg.Add(1)
		go printPhone(i+1, phone, &wg)
		wg.Wait()
	}

	fmt.Println("============================")
	fmt.Println()

	// soal 2
	fmt.Println("Soal 2")

	var movies = []string{"Harry Potter", "LOTR", "SpiderMan", "Logan", "Avengers", "Insidious", "Toy Story"}

	moviesChannel := make(chan string, len(movies))

	go getMovies(moviesChannel, movies...)

	fmt.Println("List Movies:")
	i := 1
	for value := range moviesChannel {
		fmt.Printf("%d. %s\n", i, value)
		i++
	}

	fmt.Println("============================")
	fmt.Println()

	// soal 3
	fmt.Println("Soal 3")

	chKelilingLingk := make(chan float64)
	chLuasLingk := make(chan float64)
	chVolumeTab := make(chan float64)

	go hitungKelilingLingkaran(chKelilingLingk, 8)
	fmt.Println("Keliling lingkaran dengan jari-jari 8 adalah", <-chKelilingLingk)

	go hitungLuasLingkaran(chLuasLingk, 8)
	fmt.Println("Luas lingkaran dengan jari-jari 8 adalah", <-chLuasLingk)

	go hitungVolumeTabung(chVolumeTab, 8, 10)
	fmt.Println("Volume tabung dengan jari-jari 8 dan tinggi 10 adalah", <-chVolumeTab)

	fmt.Println()

	go hitungKelilingLingkaran(chKelilingLingk, 14)
	fmt.Println("Keliling lingkaran dengan jari-jari 14 adalah", <-chKelilingLingk)

	go hitungLuasLingkaran(chLuasLingk, 14)
	fmt.Println("Luas lingkaran dengan jari-jari 14 adalah", <-chLuasLingk)

	go hitungVolumeTabung(chVolumeTab, 14, 10)
	fmt.Println("Volume tabung dengan jari-jari 14 dan tinggi 10 adalah", <-chVolumeTab)

	fmt.Println()

	go hitungKelilingLingkaran(chKelilingLingk, 20)
	fmt.Println("Keliling lingkaran dengan jari-jari 20 adalah", <-chKelilingLingk)

	go hitungLuasLingkaran(chLuasLingk, 20)
	fmt.Println("Luas lingkaran dengan jari-jari 20 adalah", <-chLuasLingk)

	go hitungVolumeTabung(chVolumeTab, 20, 10)
	fmt.Println("Volume tabung dengan jari-jari 20 dan tinggi 10 adalah", <-chVolumeTab)

	fmt.Println("============================")
	fmt.Println()

	// soal 4
	fmt.Println("Soal 4")

	chKelilingPP := make(chan int)
	go hitungKelilingPersegiPanjang(chKelilingPP, 10, 6)

	chLuasPP := make(chan int)
	go hitungLuasPersegiPanjang(chLuasPP, 10, 6)

	chVolumeBal := make(chan int)
	go hitungVolumeBalok(chVolumeBal, 10, 6, 4)

	for i = 0; i < 3; i++ {
		select {
		case keliling := <-chKelilingPP:
			fmt.Println("Keliling persegi panjang dengan panjang 10 dan lebar 6 adalah", keliling)
		case luas := <-chLuasPP:
			fmt.Println("Luas persegi panjang dengan panjang 10 dan lebar 6 adalah", luas)
		case volume := <-chVolumeBal:
			fmt.Println("Volume balok dengan panjang 10, lebar 6, dan tinggi 4 adalah", volume)
		}
	}

}
