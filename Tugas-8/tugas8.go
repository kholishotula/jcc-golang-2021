package main

import (
	"fmt"
	"math"
	"strconv"
)

// soal 1
type segitigaSamaSisi struct {
	alas, tinggi int
}

type persegiPanjang struct {
	panjang, lebar int
}

type tabung struct {
	jariJari, tinggi float64
}

type balok struct {
	panjang, lebar, tinggi int
}

type hitungBangunDatar interface {
	luas() int
	keliling() int
}

type hitungBangunRuang interface {
	volume() float64
	luasPermukaan() float64
}

func (s segitigaSamaSisi) luas() int {
	return s.alas * s.tinggi / 2
}

func (s segitigaSamaSisi) keliling() int {
	return s.alas * 3
}

func (pp persegiPanjang) luas() int {
	return pp.panjang * pp.lebar
}

func (pp persegiPanjang) keliling() int {
	return 2 * (pp.panjang + pp.lebar)
}

func (t tabung) volume() float64 {
	return math.Pi * t.jariJari * t.jariJari * t.tinggi
}

func (t tabung) luasPermukaan() float64 {
	return (2 * math.Pi * t.jariJari * t.jariJari) +
		(2 * math.Pi * t.jariJari * t.tinggi)
}

func (b balok) volume() float64 {
	return float64(b.panjang) * float64(b.lebar) * float64(b.tinggi)
}

func (b balok) luasPermukaan() float64 {
	return float64(2*b.panjang*b.lebar) +
		float64(2*b.panjang*b.tinggi) +
		float64(2*b.lebar*b.tinggi)
}

// soal 2
type phone struct {
	name, brand string
	year        int
	colors      []string
}

type printAll interface {
	printDesc() string
}

func (p phone) printDesc() string {
	var colors string
	for _i, color := range p.colors {
		colors = colors + color
		if _i != len(p.colors)-1 {
			colors = colors + ", "
		}
	}
	return "name : " + p.name +
		"\nbrand : " + p.brand +
		"\nyear: " + strconv.Itoa(p.year) +
		"\ncolor: " + colors
}

// soal 3
func luasPersegi(sisi int, param bool) interface{} {
	if param == true {
		if sisi == 0 {
			return "Maaf anda belum menginput sisi dari persegi"
		}
		return "Luas persegi dengan sisi " + strconv.Itoa(sisi) + " cm adalah " + strconv.Itoa(sisi*sisi) + " cm2"
	} else {
		if sisi == 0 {
			return nil
		}
		return strconv.Itoa(sisi * sisi)
	}
}

func main() {
	// soal 1
	fmt.Println("Soal 1")
	var bangunDatar hitungBangunDatar

	bangunDatar = segitigaSamaSisi{5, 6}
	fmt.Println("- Segitiga sama sisi")
	fmt.Println("keliling:", bangunDatar.keliling())
	fmt.Println("luas\t:", bangunDatar.luas())
	fmt.Println()
	bangunDatar = persegiPanjang{10, 4}
	fmt.Println("- Persegi Panjang")
	fmt.Println("keliling:", bangunDatar.keliling())
	fmt.Println("luas\t:", bangunDatar.luas())
	fmt.Println()

	var bangunRuang hitungBangunRuang

	bangunRuang = tabung{10, 20}
	fmt.Println("- Tabung")
	fmt.Println("luas permukaan\t:", bangunRuang.luasPermukaan())
	fmt.Println("volume\t\t:", bangunRuang.volume())
	fmt.Println()
	bangunRuang = balok{10, 4, 8}
	fmt.Println("- Balok")
	fmt.Println("luas permukaan\t:", bangunRuang.luasPermukaan())
	fmt.Println("volume\t\t:", bangunRuang.volume())

	fmt.Println("==========================================")
	fmt.Println()

	// soal 2
	fmt.Println("Soal 2")
	var cellphone phone
	cellphone.name = "Samsung Galaxy Note 20"
	cellphone.brand = "Samsung Galaxy Note 20"
	cellphone.year = 2020
	cellphone.colors = append(cellphone.colors, "Mystic Bronze")
	cellphone.colors = append(cellphone.colors, "Mystic White")
	cellphone.colors = append(cellphone.colors, "Mystic Black")

	var printing printAll
	printing = cellphone

	fmt.Println(printing.printDesc())

	fmt.Println("==========================================")
	fmt.Println()

	// soal 3
	fmt.Println("Soal 3")
	fmt.Println(luasPersegi(4, true))
	fmt.Println(luasPersegi(8, false))
	fmt.Println(luasPersegi(0, true))
	fmt.Println(luasPersegi(0, false))

	fmt.Println("==========================================")
	fmt.Println()

	// soal 4
	fmt.Println("Soal 4")
	var prefix interface{} = "hasil penjumlahan dari "
	var kumpulanAngkaPertama interface{} = []int{6, 8}
	var kumpulanAngkaKedua interface{} = []int{12, 14}

	var jumlah = 0
	jumlah += kumpulanAngkaPertama.([]int)[0]
	jumlah += kumpulanAngkaPertama.([]int)[1]
	jumlah += kumpulanAngkaKedua.([]int)[0]
	jumlah += kumpulanAngkaKedua.([]int)[1]

	fmt.Printf(prefix.(string))
	fmt.Printf(strconv.Itoa(kumpulanAngkaPertama.([]int)[0]) + " + " + strconv.Itoa(kumpulanAngkaPertama.([]int)[1]))
	fmt.Printf(" + " + strconv.Itoa(kumpulanAngkaKedua.([]int)[0]) + " + " + strconv.Itoa(kumpulanAngkaKedua.([]int)[1]))
	fmt.Printf(" = " + strconv.Itoa(jumlah))
	fmt.Println()
}
