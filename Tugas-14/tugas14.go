package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"
)

type NilaiMahasiswa struct {
	Nama        string `json:"nama"`
	MataKuliah  string `json:"matkul"`
	IndeksNilai string `json:"indeks"`
	Nilai       uint   `json:"nilai"`
	ID          uint   `json:"id"`
}

var nilaiNilaiMahasiswa = []NilaiMahasiswa{}

// cek autentikasi
func Auth(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		uname, pwd, ok := r.BasicAuth()
		if !ok {
			w.Write([]byte("Username atau Password tidak boleh kosong"))
			return
		}

		if uname == "admin" && pwd == "admin" {
			next.ServeHTTP(w, r)
			return
		}
		w.Write([]byte("Username atau Password tidak sesuai"))
		return
	})
}

// membuat dan menambahkan nilai mahasiswa
func createNilai(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	var nilaiMhs NilaiMahasiswa
	if r.Method == "POST" {
		if r.Header.Get("Content-Type") == "application/json" {
			decodeJSON := json.NewDecoder(r.Body)
			if err := decodeJSON.Decode(&nilaiMhs); err != nil {
				log.Fatal(err)
			}
		} else {
			nama := r.PostFormValue("nama")
			matkul := r.PostFormValue("matkul")
			nilai, _ := strconv.Atoi(r.PostFormValue("nilai"))

			var indeks string
			if nilai > 100 {
				w.Write([]byte("Nilai yang diisi tidak boleh melebihi 100"))
				return
			} else if nilai >= 80 {
				indeks = "A"
			} else if nilai >= 70 {
				indeks = "B"
			} else if nilai >= 60 {
				indeks = "C"
			} else if nilai >= 50 {
				indeks = "D"
			} else {
				indeks = "E"
			}

			nilaiMhs = NilaiMahasiswa{
				Nama:        nama,
				MataKuliah:  matkul,
				Nilai:       uint(nilai),
				IndeksNilai: indeks,
				ID:          uint(len(nilaiNilaiMahasiswa)) + 1,
			}

			nilaiNilaiMahasiswa = append(nilaiNilaiMahasiswa, nilaiMhs)
		}
		dataNilai, _ := json.Marshal(nilaiMhs)
		w.Write(dataNilai)
		return
	}
	http.Error(w, "NOT FOUND", http.StatusNotFound)
	return
}

// mendapatkan keseluruhan data nilai mahasiswa
func getNilai(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		nilaiMhs := nilaiNilaiMahasiswa
		dataNilai, err := json.Marshal(nilaiMhs)

		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		w.Write(dataNilai)
		return
	}
	http.Error(w, "ERROR....", http.StatusNotFound)
}

func main() {
	server := &http.Server{
		Addr: ":8080",
	}

	http.Handle("/nilai/post", Auth(http.HandlerFunc(createNilai)))
	http.HandleFunc("/nilai/get", getNilai)

	fmt.Println("server running at http://localhost:8080")
	server.ListenAndServe()
}
