package main

import (
	"errors"
	"fmt"
	"strconv"
)

// soal 1
func printKalimat(kalimat string, tahun int) {
	fmt.Println("Soal 1")
	fmt.Println("isi kalimatnya adalah", kalimat)
	fmt.Println("isi tahunnya adalah", tahun)
	fmt.Println()
}

// soal 2
func kelilingSegitigaSamaSisi(sisi int, param bool) (string, error) {
	defer deferSoal2()
	if param == true {
		if sisi == 0 {
			return "", errors.New("Maaf anda belum menginput sisi dari segitiga sama sisi")
		}
		return "Keliling segitiga sama sisinya dengan sisi " + strconv.Itoa(sisi) + " cm adalah " + strconv.Itoa(3*sisi) + " cm", nil
	} else {
		if sisi == 0 {
			panic("ERROR")
		}
		return strconv.Itoa(3 * sisi), nil
	}
}

func hasilSoal2(kalimat string, err error) {
	if err == nil {
		fmt.Println(kalimat)
	} else {
		fmt.Println(err.Error())
	}
}

func deferSoal2() {
	msg := recover()
	if msg != nil {
		fmt.Println("Terjadi panic", msg, "dan sudah di-recover")
		fmt.Println(errors.New("Maaf anda belum menginput sisi dari segitiga sama sisi"))
	}
}

// soal 3
func tambahAngka(number int, angka *int) {
	*angka += number
}

func printAngka(angka *int) {
	fmt.Println("Soal 3")
	fmt.Println("Total angka:", *angka)
	fmt.Println()
}

func main() {
	// soal 4
	angka := 1
	defer printAngka(&angka)

	// soal 1
	defer printKalimat("Candradimuka Jabar Coding Camp", 2021)

	// soal 2
	fmt.Println("Soal 2")
	fmt.Println("-----1")
	str, err := kelilingSegitigaSamaSisi(4, true)
	hasilSoal2(str, err)
	fmt.Println("-----2")
	str, err = kelilingSegitigaSamaSisi(8, false)
	hasilSoal2(str, err)
	fmt.Println("-----3")
	str, err = kelilingSegitigaSamaSisi(0, true)
	hasilSoal2(str, err)
	fmt.Println("-----4")
	str, err = kelilingSegitigaSamaSisi(0, false)
	hasilSoal2(str, err)

	// soal 3
	tambahAngka(7, &angka)
	tambahAngka(6, &angka)
	tambahAngka(-1, &angka)
	tambahAngka(9, &angka)
}
